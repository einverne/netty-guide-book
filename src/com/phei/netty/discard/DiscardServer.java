package com.phei.netty.discard;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscardServer {

  private static final Logger logger = LoggerFactory.getLogger(DiscardServer.class);
  private int port;

  public DiscardServer(int port) {
    this.port = port;
  }

  public static void main(String[] args) {
    int port;
    if (args.length > 0) {
      port = Integer.parseInt(args[0]);
    } else {
      port = 8080;
    }
    logger.info("now server listen at : " + port);
    new DiscardServer(port).run();
  }

  private void run() {
    // NioEventLoopGroup 是多线程时间循环，用来处理 IO
    // 创建两个 Group 用来处理，Boss 接受传入连接
    NioEventLoopGroup bossGroup = new NioEventLoopGroup();
    // worker 处理接受的链接，一旦 boss 接受链接，将接受的链接注册到 worker
    NioEventLoopGroup workerGroup = new NioEventLoopGroup();
    try {
      // helper class to set up a server
      ServerBootstrap b = new ServerBootstrap();
      b.group(bossGroup, workerGroup)
          .channel(NioServerSocketChannel.class)
          .childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
              socketChannel.pipeline().addLast(new DiscardServerHandler());
            }
          })
          // option() is for the NioServerSocketChannel that accepts incoming connections
          .option(ChannelOption.SO_BACKLOG, 128)
          // childOption() is for the Channels accepted by the parent ServerChannel, which is NioServerSocketChannel in this case
          .childOption(ChannelOption.SO_KEEPALIVE, true);

      ChannelFuture f = b.bind(port).sync();
      f.channel().closeFuture().sync();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      workerGroup.shutdownGracefully();
      bossGroup.shutdownGracefully();
    }
  }
}
