/*
 * Copyright 2013-2018 Lilinfeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.phei.netty.nio;

import java.io.IOException;

/**
 * NIO 实现复杂度要远远超过同步阻塞IO，但是 NIO 有如下优点：
 *
 * <ul>
 * <li>客户端发起连接操作是异步的</li>
 * <li>SocketChannel 读写操作都是异步的</li>
 * <li>JDK Selector 在 Linux 上通过 epoll 实现，没有连接句柄数限制，一个 Selector 可以同时处理成千上万个客户端连接，而且性能不会随着客户端数量增加而下降</li>
 * </ul>
 *
 * @author lilinfeng
 * @version 1.0
 * @date 2014年2月14日
 */
public class NonBlockingTimeServer {

  /**
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    int port = 8080;
    if (args != null && args.length > 0) {
      try {
        port = Integer.valueOf(args[0]);
      } catch (NumberFormatException e) {
        // 采用默认值
      }
    }
    MultiplexerTimeServer timeServer = new MultiplexerTimeServer(port);
    new Thread(timeServer, "NIO-MultiplexerTimeServer-001").start();
  }
}
