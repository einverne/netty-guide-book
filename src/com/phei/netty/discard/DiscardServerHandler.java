package com.phei.netty.discard;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.ReferenceCountUtil;

/**
 * 实现 <a href="https://tools.ietf.org/html/rfc863">DISCARD</a> 协议
 * 继承自 ChannelHandlerAdapter 提供了不同的方法可以重载
 *
 * @author einverne
 */
public class DiscardServerHandler extends ChannelHandlerAdapter {

  @Override
  public void channelActive(final ChannelHandlerContext ctx) throws Exception {
    super.channelActive(ctx);
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    // 接收到客户端消息时会被调用，传入的格式 ByteBuf
//    ByteBuf b = (ByteBuf) msg;
//    try {
//      while (b.isReadable()) {
//        System.out.println((char) b.readByte());
//        System.out.flush();
//      }
//    } finally {
//      ReferenceCountUtil.release(msg);
////       or
////       b.release();
//    }
    ctx.writeAndFlush(msg);

  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}
