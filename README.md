# netty-book-2

2018-10-24 更新

在原始[项目](https://github.com/wuyinxian124/nettybook2)的基础上根据[codestyle](https://einverne.gitbooks.io/intellij-idea-tutorial/content/codestype-settings.html)格式化了代码，增加了一些注释。

# 原始说明
该项目是李林峰老师编写的netty权威指南（第二版）对应的源码。
源码原始地址是：<http://vdisk.weibo.com/s/C9LV9iVqAFvqu>

因为原始项目是用ant构建，而且还要下载导入。所以本人将其项目进行简单的maven转换，并且提交到github上。这样同志们就可以直接import -> git查看。（当然一些有关更ant的操作，本人没有修改，大神勿喷）

当然这些工作得到的李林峰老师的同意和认可，不信，你看：<http://weibo.com/1725503810/Ci8nIa5IQ?type=comment>

有关该书的更多信息可以关注李林峰老师的微博 @Nettying 以及查看其在ifeve网站上的文章：http://ifeve.com/author/linfeng/


